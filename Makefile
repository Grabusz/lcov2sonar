# TODO: Autotools
# TODO: Standalone converter

CXX = g++
LD = g++

CXXFLAGS += -shared -O2 -s -fPIC -std=c++11 -Wall
LDFLAGS += -shared -fPIC

install: lcov2sonar.so

lcov2sonar.so: src/lcov2sonar.o
	$(LD) $< -o $@ $(LDFLAGS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
