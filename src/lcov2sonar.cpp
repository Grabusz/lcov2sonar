#include <cstdio>
#include <string>
#include <cstdlib>
#include <ostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <unordered_map>
#include <cstring>
#include <iostream>

typedef uintmax_t BranchNo;
typedef uintmax_t Executed;
typedef uintmax_t LineNo;

struct Line {
	std::vector<std::pair<BranchNo, Executed>> branches;
	size_t executed;
};

static void indent(
		std::ostream &out,
		const size_t indent,
		const char *style,
		const bool readable) {
	if(readable) {
		const size_t len = strlen(style);
		out.write("\n", 1);
		for(size_t i=indent; i; --i)
			out.write(style, len);
	}
}

static std::string parseDirPath(const std::string &string) {
	std::string path;
	path.reserve(string.size());
	path += string[0];
	auto prevDirSeparator(string.cbegin());
	for(auto it = string.cbegin()+1; it != string.cend(); ++it) {
		if(*it == '/') {
			if(prevDirSeparator+1 != it)
				path += *it;
			prevDirSeparator = it;
		} else
			path += *it;
	}

	if(path[path.length()-1] != '/')
		path += '/';

	return path;
}

// TODO: Windows dir separator compatibility
static std::string getRelativeFrom(const std::string &from, const std::string &to) {
	// NOTE: Assumes 'to' is a file and 'from' is a directory
	if(from == "")
		return to;
	if(to == "")
		return "";

	if(from[0] != '/')
		throw "From doesn\'t start at root";
	if(to.length() > 1 && to[to.length()-1] == '/')
		throw "From isn't a path to a file";

	if(from.rfind('/') == std::string::npos)
		throw "From has no directories";

	std::string localFrom = parseDirPath(from);
	std::string localTo = parseDirPath(to);
	localTo.erase(localTo.length()-1);

	auto fromPtr = localFrom.cbegin();
	auto fromEnd = localFrom.cend();
	auto toPtr = localTo.cbegin();
	auto toEnd = localTo.cend();
	auto lastDirSeparator = fromPtr;
	while(fromPtr != fromEnd && toPtr != toEnd) {
		if(*fromPtr == *toPtr) {
			if(*fromPtr == '/')
				lastDirSeparator = fromPtr;
		} else
			break;
		++fromPtr;
		++toPtr;
	}

	std::string relativePath;

	size_t nextDir = fromPtr - localFrom.cbegin()-1;
	while((nextDir = localFrom.find('/', nextDir+1)) != std::string::npos)
		relativePath.append("../");

	relativePath.append(localTo.cbegin() + (lastDirSeparator - localFrom.cbegin() + 1), toEnd);
	return relativePath;
}

static void writeFile(
		std::ostream &out,
		const std::string &filename,
		const std::unordered_map<LineNo, Line> &lines,
		const bool readable) {
	if(filename == "")
		return;

	indent(out, 1, "  ", readable);
	out.write("<file path=\"", strlen("<file path=\""));
	out.write(filename.c_str(), filename.length());
	out.write("\">", strlen("\">"));
	for(auto &line : lines) {
		indent(out, 2, "  ", readable);
		out.write("<lineToCover lineNumber=\"", strlen("<lineToCover lineNumber=\""));
		std::string temp = std::to_string(line.first);
		out.write(temp.c_str(), temp.length());
		if(line.second.executed == 0)
			out.write(
				"\" covered=\"false\"",
				strlen("\" covered=\"false\""));
		else
			out.write(
				"\" covered=\"true\"",
				strlen("\" covered=\"true\""));

		if(line.second.branches.size()) {
			temp = std::to_string(line.second.branches.size());
			out.write(" branchesToCover=\"", strlen(" branchesToCover=\""));
			out.write(temp.c_str(), temp.length());
			out.write("\" coveredBranches=\"", strlen("\" coveredBranches=\""));

			BranchNo covered = 0;
			for(auto &branch : line.second.branches)
				if(branch.second != 0)
					++covered;

			temp = std::to_string(covered);
			out.write(temp.c_str(), temp.length());
			out.write("\"", strlen("\""));
		}

		out.write("/>", strlen("/>"));
	}
	indent(out, 1, "  ", readable);
	out.write("</file>", strlen("</file>"));
}

void lcov2sonar(
		const std::string &lcovFile,
		const std::string &projectRoot,
		std::ostream &out,
		const bool readable = true) {
	if(!out.good())
		throw "Output stream is not in good state";

	std::string filename("");
	std::unordered_map<LineNo, Line> lines;

	std::ifstream in(lcovFile.c_str());
	if(!in.is_open())
		throw "Failed to open coverage file";

	char start[] = "<coverage version=\"1\">";
	out.write(start, strlen(start));

	for(std::string line; std::getline(in, line);) {
		if(strncmp(line.c_str(), "SF:", strlen("SF:")) == 0) {
			// Dump collected data to out
			writeFile(out, getRelativeFrom(projectRoot, filename), lines, readable);

			// Clear data for new file pass
			filename.assign(line.c_str() + strlen("SF:"));
			lines.clear();
		} else if(strncmp(line.c_str(), "BRDA:", strlen("BRDA:")) == 0) {
			// Branch coverage
			char buf[2];
			LineNo l;
			BranchNo b;
			char eBuf[23];
			eBuf[22] = '\0';
			sscanf(line.c_str() + strlen("BRDA:"), "%zu,%1s,%zu,%22s", &l, buf, &b, eBuf);

			Line _l;
			_l.executed = 0;
			Line *lineStat = &_l;
			auto oldStats = lines.find(l);
			if(oldStats != lines.end())
				lineStat = &oldStats->second;

			if(eBuf[0] == '-')
				lineStat->branches.emplace_back(b, 0);
			else
				lineStat->branches.emplace_back(b, strtoull(eBuf, nullptr, 10));

			if(oldStats == lines.end())
				lines.emplace(l, *lineStat);
		} else if(strncmp(line.c_str(), "DA:", strlen("DA:")) == 0) {
			// Line coverage
			LineNo l;
			Executed e;
			sscanf(line.c_str() + strlen("DA:"), "%zu,%zu", &l, &e);

			Line _l;
			Line *lineStat = &_l;
			auto oldStats = lines.find(l);
			if(oldStats != lines.end())
				lineStat = &oldStats->second;

			lineStat->executed = e;
			if(oldStats == lines.end())
				lines.emplace(l, *lineStat);
		}
	}

	writeFile(out, getRelativeFrom(projectRoot, filename), lines, readable);

	indent(out, 0, "  ", readable);
	char end[] = "</coverage>\n";
	out.write(end, strlen(end));
	out.flush();
}
